/* Orx - Portable Game Engine
*
* Copyright (c) 2008-2017 Orx-Project
*
* This software is provided 'as-is', without any express or implied
* warranty. In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
*
*    1. The origin of this software must not be misrepresented; you must not
*    claim that you wrote the original software. If you use this software
*    in a product, an acknowledgment in the product documentation would be
*    appreciated but is not required.
*
*    2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
*
*    3. This notice may not be removed or altered from any source
*    distribution.
*/

/**
* @file MyGame.cpp
* @date 17/05/2017
* @author sausage@zeta.org.au
*
* Orx Game template for Visual Studio 2015 32-bit
*/


#include "orx.h"
orxOBJECT *ufo;
orxOBJECT *ufoYouWinTextObject;
//orxOBJECT *scoreObject;
orxCAMERA *camera;
//int score = 0;

/* This is a basic C++ template to quickly and easily dive into a project or tutorial.
*
* As orx is data driven, here we just write 2 lines of code to create a viewport
* and an object. All their properties are defined in the config file (01_Object.ini).
* As a matter of fact, the viewport is associated with a camera implicitly created from the
* info given in the config file. You can also set their sizes, positions, the object colors,
* scales, rotations, animations, physical properties, and so on. You can even request
* random values for these without having to add a single line of code.
* In a later tutorial we'll see how to generate your whole scene (all background
* and landscape objects for example) with a simple for loop written in 3 lines of code.
*
* For now, you can try to uncomment some of the lines of 01_Object.ini, play with them,
* then relaunch this tutorial. For an exhaustive list of options, please look at CreationTemplate.ini.
*/

void orxFASTCALL Update(const orxCLOCK_INFO *_pstClockInfo, void *_pContext)
{

	if (ufo) {

		const orxFLOAT FORCE = 1.8;

		orxVECTOR rightForce = { FORCE, 0, 0 };
		orxVECTOR leftForce = { -FORCE, 0, 0 };
		orxVECTOR upForce = { 0, -FORCE, 0 };
		orxVECTOR downForce = { 0, FORCE, 0 };


		if (orxInput_IsActive("GoLeft")) {
			orxObject_ApplyForce(ufo, &leftForce, orxNULL);
		}
		if (orxInput_IsActive("GoRight")) {
			orxObject_ApplyForce(ufo, &rightForce, orxNULL);
		}
		if (orxInput_IsActive("GoUp")) {
			orxObject_ApplyForce(ufo, &upForce, orxNULL);
		}
		if (orxInput_IsActive("GoDown")) {
			orxObject_ApplyForce(ufo, &downForce, orxNULL);
		}
	}

	//if (scoreObject) {
	//	orxCHAR formattedScore[5];
	//	orxString_Print(formattedScore, "%d", score);

	//	orxObject_SetTextString(scoreObject, formattedScore);
	//}
}

orxSTATUS orxFASTCALL PhysicsEventHandler(const orxEVENT *_pstEvent)
{
	if (_pstEvent->eType == orxEVENT_TYPE_PHYSICS) {

		if (_pstEvent->eID == orxPHYSICS_EVENT_CONTACT_ADD) {
			orxOBJECT *pstRecipientObject, *pstSenderObject;

			/* Gets colliding objects */
			pstRecipientObject = orxOBJECT(_pstEvent->hRecipient);
			pstSenderObject = orxOBJECT(_pstEvent->hSender);

			const orxSTRING recipientName = orxObject_GetName(pstRecipientObject);
			const orxSTRING senderName = orxObject_GetName(pstSenderObject);

			if (orxString_Compare(senderName, "UfoObject") == 0) {
				if (orxString_SearchString(recipientName, "PickupObject") != orxNULL) {
					orxObject_SetLifeTime(pstRecipientObject, 0);
					orxObject_AddSound(pstSenderObject, "PickupSound");
					//score += 150;
				}
			}

			if (orxString_Compare(recipientName, "UfoObject") == 0) {
				if (orxString_SearchString(senderName, "PickupObject") != orxNULL) {
					orxObject_SetLifeTime(pstSenderObject, 0);
					orxObject_AddSound(pstRecipientObject, "PickupSound");
					//score += 150;
				}
			}

			//if (orxObject_IsEnabled(ufoYouWinTextObject) == orxFALSE && score == 1200) {
			//	orxObject_Enable(ufoYouWinTextObject, orxTRUE);
			//}
		}
	}

	return orxSTATUS_SUCCESS;
}

/** Initialises your game
*/
orxSTATUS orxFASTCALL Init()
{
	/* Displays a small hint in console */
	orxLOG("\n* This tutorial creates a viewport/camera couple and an object"
		"\n* You can play with the config parameters in ../MyGame.ini"
		"\n* After changing them, relaunch the tutorial to see the changes.");

	/* Creates viewport */
	orxVIEWPORT *viewport = orxViewport_CreateFromConfig("Viewport");
	camera = orxViewport_GetCamera(viewport);


	/* Creates object */
	orxObject_CreateFromConfig("BackgroundObject");
	
	ufo = orxObject_CreateFromConfig("UfoObject");
	orxCamera_SetParent(camera, ufo);


	//orxObject_CreateFromConfig("PickupObjects");

	//scoreObject = orxObject_CreateFromConfig("ScoreObject");
	ufoYouWinTextObject = orxObject_CreateFromConfig("YouWinObject");
	orxObject_SetParent(ufoYouWinTextObject, ufo);

	//orxObject_Enable(ufoYouWinTextObject, orxFALSE);


	orxClock_Register(orxClock_FindFirst(orx2F(-1.0f), orxCLOCK_TYPE_CORE), Update, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);
	//orxEvent_AddHandler(orxEVENT_TYPE_PHYSICS, PhysicsEventHandler);
	/* Done! */
	return orxSTATUS_SUCCESS;
}

/** Run function, is called every clock cycle
*/
orxSTATUS orxFASTCALL Run()
{
	orxSTATUS eResult = orxSTATUS_SUCCESS;

	/* Should quit? */
	if (orxInput_IsActive("Quit"))
	{
		/* Updates result */
		eResult = orxSTATUS_FAILURE;
	}

	/* Done! */
	return eResult;
}

/** Exit function
*/
void orxFASTCALL Exit()
{
	/* We're a bit lazy here so we let orx clean all our mess! :) */
}

/** Main function
*/
int main(int argc, char **argv)
{
	/* Executes a new instance of tutorial */
	orx_Execute(argc, argv, Init, Run, Exit);

	return EXIT_SUCCESS;
}


#ifdef __orxMSVC__

// Here's an example for a console-less program under windows with visual studio
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	// Inits and executes orx
	orx_WinExecute(Init, Run, Exit);

	// Done!
	return EXIT_SUCCESS;
}

#endif // __orxMSVC__
